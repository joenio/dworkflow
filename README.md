# dworkflow - debian workflow

dworkflow is a collection of scripts that reflects my workflow on
debian packaging and other debian contribution tasks

## quickstart

```sh
dw check
dw build
dw test
```

## workflow

my default workflow is

1. `dw-packages`
1. `dw-new`
1. edit files `debian/control`, `debian/copyright`...
   - `dw-control`
   - `dw-copyright`
1. `dw-build`
1. `dw-lintian`
1. `dw-autopkgtest`
   - `dw-autopkgtest qemu`
   - `dw-autopkgtest lxc`
1. fix any error before the next steps
1. `dw-itp`
1. `reportbug -r /tmp/mail`
1. wait for the email with the bug number
1. add the bug number to `debian/changelog`
   - `Initial release. (Closes: #1069187)`
1. git push to salsa
1. `dw-upload` - upload source package to deb.4two.art
1. `dch -r`
   - set `debian/changelog` to unstable
   - update datetime
1. `dw-debsign`
   - opengpg sign source .cnanges file
1. `dw-dput`
   - upload to mentors.debian.net
   - then ask some DD to review the package
   - optionally open a RFS bug

## author

Joenio Marques da Costa <joenio@joenio.me>

## license

This project is licensed under the GNU General Public License v3.0 or later.
