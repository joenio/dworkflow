#!/bin/sh

VERSION=$(dw-version)
SRCNAME=$(dw-srcname)
printf "[dw-dput] dput mentors ../${SRCNAME}_${VERSION}_source.changes\n"
dput $@ mentors ../${SRCNAME}_${VERSION}_source.changes

: <<=cut
=head1 NAME

dw-dput - upload source package to mentors.debian.net

=head1 SYNOPSIS

   dw-dput [dput options]

=head1 OPTIONS

the dput's option C<--force> can be used to force re-upload the package to
mentors

=head1 DESCRIPTION

run C<dput> to upload package to L<mentors.debian.net> in order to ask
sponsoring RFS and review of some DD with upload rights before uploading the
package to the Debian archive

the following command is used to upload the package to mentors

    dput mentors ../${SRCNAME}_${VERSION}_source.changes

this script is part of the
L<joenio's debian workflow|https://gitlab.com/joenio/debian-dev>

=head1 DPUT CONFIGURATION

before running this script you must make sure you have the configuration below
in your C<$HOME/.dput.cf> file

    [mentors]
    fqdn = mentors.debian.net
    incoming = /upload
    method = https
    allow_unsigned_uploads = 0
    progress_indicator = 2
    # Allow uploads for UNRELEASED packages
    allowed_distributions = .*

=head1 SEE ALSO

L<https://mentors.debian.net/intro-maintainers/>

=head1 AUTHOR

Joenio Marques da Costa <joenio@joenio.me>

=head1 LICENSE

This project is licensed under the GNU General Public License v3.0 or later.

=cut
