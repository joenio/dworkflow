help:
	@echo "dworkflow makefile:"
	@echo ""
	@echo "  make install - install binaries under ~/.local/bin"

install:
	nodev install-bin
